# Blockchain Papers

This repository contains a collection of papers from various conferences and journals, with a primary focus on the implementation of design patterns and testing methodologies in blockchain-based applications.

# Repos and Resources for DApp Design Patterns

## Pattern collections

* https://github.com/fravoll/solidity-patterns - Collection of design and programming patterns for DApps

* https://github.com/cjgdev/smart-contract-patterns - Collection of Solidity patterns

* https://www.slideshare.net/mids106/dapp-design-patterns - Slides detailing different patterns for DApps

* http://dapp.tools/dappsys/ - Collection of audited building blocks for DApps

* https://github.com/ConsenSys/smart-contract-best-practices/blob/master/docs/recommendations.md

* https://medium.com/@mohan.venkataraman/using-patterns-in-coding-smart-contracts-412547051cb9

### Oracle/Verifier Pattern

* https://github.com/blockchain-certificates/cert-verifier-js

* https://github.com/runtimeverification/verified-smart-contracts

* https://github.com/austintgriffith/concurrence.io

## Open source DApps that use Design Patterns

* https://github.com/maxwoe/solidity_patterns - Implementation of all design patterns in the paper [Smart Contracts: Security Patterns in the Ethereum Ecosystem and Solidity](https://bitbucket.org/ChequeChain/blockchainpapers/src/master/Design_Testing_Papers/Design_Pattern_Implementation/2018_Smart%20Contracts%20Security%20Patterns%20in%20the%20Ethereum%20Ecosystem%20and%20Solidity.pdf)

* https://github.com/pedroduartecosta/blockchain-oracle: Uses Oracle design pattern 

## Open source DApps that may or may not have Design patterns

* https://github.com/instamed/healthcare-payments-blockchain - Healthcare Blockchain with live demo https://blockchain-demo.instamed.com/provider

* https://github.com/Kerala-Blockchain-Academy/AgroChain - Ethereum DApp for creating a mutual trust agreement between farmers and consumers

* https://github.com/pblin/blockchain-in-campaign-finance - Ethereum DApp for election campaign funding

# Repos for Solidity Quality Metrics

## Static Analysis

https://github.com/crytic/slither - Most popular static analyzer

https://github.com/chicxurug/SolMet-Solidity-parser

https://github.com/aphd/solidity-metrics